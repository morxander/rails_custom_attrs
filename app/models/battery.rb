class Battery < ApplicationRecord
  include AttributesConcern
  ADDITIONAL_ATTRS = %w[brand price].freeze
end
