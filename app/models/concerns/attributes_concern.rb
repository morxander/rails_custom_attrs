module AttributesConcern
  extend ActiveSupport::Concern

  included do
    before_save :check_for_attributes
    after_save :update_record_id
    attr_accessor :attributes_array
  end

  def set_attribute(key, value)
    # Check if it's a valid attribute
    raise ApplicationController::UnknownAttribute unless valid_attribute? key
    # Create or update
    attribute = Attribute.find_or_create_by(record_id: self.id, model_type: self.class.name, attr_key: key)
    attribute.update(attr_value: value)
    self.attributes_array = [] if self.attributes_array.blank?
    self.attributes_array.append(attribute) if self.new_record?
  end

  def get_attribute(key)
    # Check if it's a valid attribute
    raise ApplicationController::UnknownAttribute unless valid_attribute? key
    # Get the attribute record
    attribute = Attribute.where(record_id: self.id, model_type: self.class.name, attr_key: key)
    return nil if attribute.blank?
    attribute.first.attr_value
  end

  def attributes
    Attribute.where(record_id: self.id,
                    model_type: self.class.name)
        .pluck(:attr_key, :attr_value).as_json.to_h
  end

  def valid_attribute?(key)
    self.class::ADDITIONAL_ATTRS.include? key
  end

  def check_for_attributes
    # Clear the attributes errors
    self.errors[:attributes].clear
    # Check for the attributes
    self.class::ADDITIONAL_ATTRS.each do |key|
      attribute = get_attribute key
      if attribute.blank?
        self.errors.add(:attributes, "#{key} attribute is missing")
        throw(:abort)
      end
    end
    true
  end

  def update_record_id
    self.attributes_array.each do |attribute|
      attribute.update(record_id: self.id)
    end
  end

end