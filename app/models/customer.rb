class Customer < ApplicationRecord
  include AttributesConcern
  ADDITIONAL_ATTRS = %w[email address].freeze
end
