class ApplicationController < ActionController::Base

  # Customer Errors
  class UnknownAttribute < StandardError; end

end
