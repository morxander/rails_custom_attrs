class CreateAttributes < ActiveRecord::Migration[5.2]
  def change
    create_table :attributes do |t|
      t.integer :record_id
      t.string :model_type
      t.string :attr_key
      t.string :attr_value

      t.timestamps
    end
  end
end
