
# Custom Attributes Project

In this `Ruby on Rails` project I used the `Concern` pattern to have custom attributes ( fields ) for any model without adding a new column every time.
I am storing the additional attributes in a `attributes` table. And every record in this table has the following :
- Model Type
- Record ID
- Attribute Key
- Attribute Value

And to use it in any model you just have to add the following two lines :

```ruby
class ExampleModel < ApplicationRecord 
 # use the Attribute Concern in your model
 include AttributesConcern
 # Define the allowd attributes for this model
 ADDITIONAL_ATTRS = %w[brand price].freeze
end
```  

Which will give you the following methods :

```ruby
example = ExampleModel.new
# To Add/Update Attribute
example.set_attribute('key', 'value')
# To get an Attribute value by key
example.get_attribute 'key'
# To get a Hash of the Attributes for this object
example.attributes
```

Please notice that you have to catch this error `ApplicationController::UnknownAttribute` while using `set_attribute()` and `get_attribute()` methods because you're already defined the allowed attributes so you can't use undefined attribute.

Also If you want to get the attributes through `SQL` you can do it :

```sql
SELECT * FROM "attributes" WHERE "attributes"."record_id" = {{RECORD_ID}} AND "attributes"."model_type" = {{MODEL_NAME}}
```

And replace the `{{RECORD_ID}}` with the record ID in its table and `{{MODEL_NAME}}` with the model name.